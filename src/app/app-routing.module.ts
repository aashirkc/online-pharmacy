import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { ContactusComponent } from './extras/contactus/contactus.component';
import { AboutusComponent } from './extras/aboutus/aboutus.component';
import { CommingsoonComponent } from './extras/commingsoon/commingsoon.component';

const routes: Routes = [
  { path: '', loadChildren: './layouts/layouts.module#LayoutsModule' },
  { path: 'login', loadChildren: './extras/login/login.module#LoginModule' },
  { path: 'signup', loadChildren: './extras/signup/signup.module#SignupModule' },
  { path: 'forgotpassword', loadChildren: './extras/forgotpassword/forgotpassword.module#ForgotpasswordModule' },

  { path: 'contact', loadChildren: './extras/contactus/contactus.module#ContactusModule' },
  { path: 'about', loadChildren: './extras/aboutus/aboutus.module#AboutusModule' },
  { path: 'commingsoon', loadChildren: './extras/commingsoon/commingsoon.module#CommingsoonModule' },
  { path: '404-not-found', loadChildren: './extras/not-found/not-found.module#NotFoundModule' },
  { path: '**', redirectTo: '404-not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
