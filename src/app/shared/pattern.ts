
/**
 *
 *
 * @export
 * @class Pattern
 */
export class Pattern {
    public static emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    public static passwordPattern = '(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*';
    public static namePattern = '[A-Za-z ]{1,32}';
    public static addressPattern = '[A-Za-z0-9\'\.\-\s\,]';
    public static phoneNumber = '[9.8][0-9]{9}';
    public static categoryPattern = '[A-Za-z0-9 ]{1,32}';
    public static number = '[0-9]{1,11}';
}
