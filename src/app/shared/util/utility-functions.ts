
export class UtilityFunctions {
    encodedImage;

    /**
     *Helps To Show the image preview
     * and as well as generate encoded image
     * 
     * @param {*} files
     * @returns
     */
    previewImage(files) {
        let encodedImage;
        // return if file is empty
        if (files.length === 0) {
            return null;
        }
        //checking if file is type of image
        if (files[0].type.match(/image\/*/) == null) {
            return null;
        }
        //create reader object
        const reader = new FileReader();
        //get file data
        reader.readAsDataURL(files[0]);
        //when file loaded to browser after user selects image
        reader.onload = (_event) => {
            //get base 64 value
            this.encodedImage = encodedImage = reader.result;
        };
        return encodedImage;
    }

    /**
     * Function to Go To Top of Page
     * Doesn't Work on popup
     *
     * @memberof UtilityFunctions
     */
    gotoTopOfPage() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    /**
     * Goto the html id specified 
     *
     * @param {string} htmlElementId
     * @memberof UtilityFunctions
     */
    gotoDefinedView(htmlElementId: string) {
        document.getElementById(htmlElementId).scrollIntoView(true);
    }

    /**
     * Get the Minimum date for the Calender to set
     *
     * @returns
     * {
     *     year: year,
     *     month: month,
     *     day: day
     *  }
     * @memberof UtilityFunctions
     */
    getMinDate() {
        const currentDate = new Date();
        const month = currentDate.getUTCMonth() + 1; //months from 1-12
        const day = currentDate.getUTCDate();
        const year = currentDate.getUTCFullYear();
        return {
            year: year,
            month: month,
            day: day
        };
    }

    /**
     * Get Date in year/month/day from any date
     *
     * @param {*} date
     * @returns
     * @memberof UtilityFunctions
     */
    getDate(date) {
        const dateObj = new Date(date);
        const month = dateObj.getUTCMonth() + 1; //months from 1-12
        const day = dateObj.getUTCDate();
        const year = dateObj.getUTCFullYear();

        let newdate = year + "/" + month + "/" + day;
        return newdate;
    }

    getNgBootStrapFormatDate(date: Date) {
        const dateObj = new Date(date);
        const year = dateObj.getUTCFullYear();
        const month = dateObj.getUTCMonth() + 1; //months from 1-12
        const day = dateObj.getUTCDate();
        return {
            year: year,
            month: month,
            day: day
        };
    }

    getNormalDateFromNgDate(date) {
        const year = date.year;
        const month = date.month;
        const day = date.day;
        const dateObj = year + "/" + month + "/" + day;
        return new Date(dateObj);
    }


    getUserType(userType) {
        if (userType === 'traveller') {
            return 'Traveller';
        }
        else if (userType === 'agency') {
            return 'Agency';

        }
        else {
            return 'Admin';
        }
    }

    getGenderType(gender: string) {
        if (gender === 'male') {
            return 'Male';
        }
        else if (gender === 'female') {
            return 'Female';

        }
        else if (gender === 'other') {
            return 'Other';

        }
        else {
            return 'Prefer Not To Say';

        }
    }
}