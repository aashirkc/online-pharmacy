import { Component, OnInit } from '@angular/core';
import { Users } from '../model/user';
import { Router } from '@angular/router';
import { StorageService } from '../services/storage/storage.service';
import { AllCategoriesServicesService } from '../services/all-categories-services.service';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public userDetails: Users = new Users();

  changeText: boolean = false;

  constructor(public router: Router,
    private storageService: StorageService,
    private all: AllCategoriesServicesService,
    private _cart: CartService,
  ) {
    this._cart.cartLen.subscribe(data=>{
      console.log(data)
      this.b = data.length
    })
   }

  show() {
    this.changeText = true;
    console.log('hey')
  }

  hide() {
    this.changeText = false;
  }
  categories: any;
  cartLen: any

  ngOnInit() {
   

    this.b = this._cart.getItemFromCart().length
     


    this.all.findAllFamousPlaces().subscribe(d => {
      this.categories = d
      console.log(this.categories)
    })

    this.storageService.watchStorage().subscribe((data: string) => {
      this.getLoggedInUserDetails();
    });
    this.getLoggedInUserDetails();

  }
  b: any

  getCartData() {
    let a = JSON.parse(localStorage.getItem('cart'))
    debugger
    this.b = a.length;


  }

  showHam() {
    document.getElementById('mySidenav').style.display = "inline";
  }
  hideHam() {
    document.getElementById('mySidenav').style.display = "none";
  }
  getLoggedInUserDetails() {
    if (localStorage.getItem('loggenInUser') == null) {
      this.userDetails = new Users();
    }
    else {
      this.userDetails = JSON.parse(atob(localStorage.getItem('loggenInUser'))) as Users;
    }
  }

  onLoggedout() {
    localStorage.removeItem('token');
    localStorage.removeItem('language');
    localStorage.removeItem('loggenInUser');
    this.userDetails = new Users();
    this.router.navigate(['/login']);
  }

}
