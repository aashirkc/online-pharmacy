import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from './api-end-points';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private http: HttpClient) { }


  getHeader() {
    const token = 'Bearer ' + localStorage.getItem('token');
    const headerData = new HttpHeaders({ 'content-Type': 'application/x-www-form-urlencoded' });
    return headerData;
  }


 
  getBanner() {
    const bannerUrl = ApiEndPoints.BANNER_ENDPOINT;

    return this.http.get(bannerUrl, { headers: this.getHeader() });
  }

  getFeaturedProduct() {
    const featuredUrl = ApiEndPoints.FEATURED_PRODUTCS;

    return this.http.get(featuredUrl, { headers: this.getHeader() });
  }

  getHotProduct() {
    const featuredUrl = ApiEndPoints.HOT_PRODUTCS;
    return this.http.get(featuredUrl, { headers: this.getHeader() });
  }
}
