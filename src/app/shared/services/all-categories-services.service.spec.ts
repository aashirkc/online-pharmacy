import { TestBed } from '@angular/core/testing';

import { AllCategoriesServicesService } from './all-categories-services.service';

describe('AllCategoriesServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllCategoriesServicesService = TestBed.get(AllCategoriesServicesService);
    expect(service).toBeTruthy();
  });
});
