import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from './api-end-points';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AllCategoriesServicesService {

  constructor(private http: HttpClient) { }

  getHeader() {
    const token = 'Bearer ' + localStorage.getItem('token');
    const headerData = new HttpHeaders({ 'content-Type': 'application/x-www-form-urlencoded' });
    return headerData;
  }

  findAllFamousPlaces() {
    const famousPlaceUrl = ApiEndPoints.FAMOUSPLACE_ALL_ENDPOINTS;

    return this.http.get(famousPlaceUrl, { headers: this.getHeader() });
  }

  findAllCaegoriesData(id) {
    const famousPlaceUrl = ApiEndPoints.CATEGORY_ENDPOINT;

    return this.http.get(famousPlaceUrl + "/" + id, { headers: this.getHeader() });
  }

  searchFamousPlace(id): Observable<any[]> {
    const searchUrl = ApiEndPoints.CATEGORY_ENDPOINT;
    return this.http.get<any[]>(searchUrl + "/" + id, { headers: this.getHeader() });
  }

  findAllLocalPlaces() {
    const localPlaceUrl = ApiEndPoints.LOCAL_LOCATIONS_ALL_ENDPOINTS;
    return this.http.get(localPlaceUrl, { headers: this.getHeader() });
  }
  findFamousPlaceById(id: number) {
    const famousPlaceUrl = ApiEndPoints.FAMOUS_VISITING_PLACE_ENDPOINT + '/' + id;
    return this.http.get(famousPlaceUrl, { headers: this.getHeader() });
  }

  findLocalLocationById(id: number) {
    const localLocationUrl = ApiEndPoints.LOCAL_LOCATIONS_ENDPOINT + '/' + id;
    return this.http.get(localLocationUrl, { headers: this.getHeader() });
  }

  findFirstAidKits() {
    const localLocationUrl = ApiEndPoints.FIRST_AID_KITS;
    return this.http.get(localLocationUrl, { headers: this.getHeader() });
  }

  findMedicine() {
    const localLocationUrl = ApiEndPoints.MEDICINE;
    return this.http.get(localLocationUrl, { headers: this.getHeader() });
  }
  findMedicineProducts(id: number) {
    const localLocationUrl = ApiEndPoints.MEDICINE + '/' + id;
    return this.http.get(localLocationUrl, { headers: this.getHeader() });
  }
  // searchFamousPlace(): Observable<FamousVisitingPlace[]>{
  //   const searchUrl = ApiEndPoints.FAMOUSPLACE_ALL_ENDPOINTS;
  //   return this.http.get<FamousVisitingPlace[]>(searchUrl,{headers:this.getHeader()});
  // }
}
