import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiEndPoints } from './api-end-points';



@Injectable({
  providedIn: 'root'
})
export class CustomService {

  constructor(private http: HttpClient) { }

  checkEmailExists(email: string) {
    const url = ApiEndPoints.CUSTOM_ENDPOINT_CHECK_EMAIL;
    return this.http.post(url, JSON.stringify(email), { headers: this.getHeader() });
  }

  getUserDetailsFromToken(token: string) {
    const url = ApiEndPoints.CUSTOM_ENDPOINT_GET_USER_DETAILS;
    return this.http.post(url, JSON.stringify(token), { headers: this.getHeader() });
  }

  private getHeader() {
    const token = 'Bearer ' + localStorage.getItem('token');
    const headerData = new HttpHeaders({ 'content-Type': 'application/x-www-form-urlencoded'});
    return headerData;
  }




  prescriptionSave(post) {
    const url = ApiEndPoints.PRESCRIPTION;

    return this.http.post(url, post,{ headers: this.getHeader() })

  }
  orderSave(post) {

    console.log(post)
    let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
    const Params = new HttpParams()
    const url = ApiEndPoints.ORDER;

    return this.http.post(url, (post),{ headers:myHeaders, params: Params})

  }

}
