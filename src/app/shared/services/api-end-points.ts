import { Inject } from '@angular/core';

export class ApiEndPoints {
  apiUrl: any
  constructor(

    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;

  }


  private static API_MAIN_ENDPOINT = 'https://api.easymedsonline.com';
  private static BASE_ENDPOINT = ApiEndPoints.API_MAIN_ENDPOINT + '/api/';

  public static SIGNUP_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'register';
  public static LOGIN_ENDPOINT = ApiEndPoints.API_MAIN_ENDPOINT + '/login';
  public static CATEGORY_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'category';
  public static BANNER_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'frontpageSlider';
  public static FEATURED_PRODUTCS = ApiEndPoints.BASE_ENDPOINT + 'featured-product';
  public static HOT_PRODUTCS = ApiEndPoints.BASE_ENDPOINT + 'hot-product';
  public static MEDICINE = ApiEndPoints.BASE_ENDPOINT + 'brands';
  public static FAMOUS_VISITING_PLACE_ENDPOINT = ApiEndPoints.BASE_ENDPOINT
  public static FIRST_AID_KITS = ApiEndPoints.BASE_ENDPOINT + 'category_post/first_aid_kits';
  public static PRESCRIPTION = ApiEndPoints.BASE_ENDPOINT + 'prescription';
  public static ORDER = ApiEndPoints.BASE_ENDPOINT + 'order';
  public static LOCAL_LOCATIONS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'locallocations';
  public static LOCAL_PRODUCTS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'localproducts';
  public static OFFERS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'offers';
  public static OWNER_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'owner';
  public static USER_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'user';
  public static LOCATION_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'location';
  public static CUSTOM_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'custom';

  // all famous places endpoints
  public static FAMOUSPLACE_ALL_ENDPOINTS = ApiEndPoints.FAMOUS_VISITING_PLACE_ENDPOINT + 'get_all_categories';

  // all local places endpoints
  public static LOCAL_LOCATIONS_ALL_ENDPOINTS = ApiEndPoints.LOCAL_LOCATIONS_ENDPOINT + '/all';



  //custom endpoints
  public static CUSTOM_ENDPOINT_CHECK_EMAIL = ApiEndPoints.CUSTOM_ENDPOINT + '/emailcheck';
  public static CUSTOM_ENDPOINT_GET_USER_DETAILS = ApiEndPoints.CUSTOM_ENDPOINT + '/getuserdetails';
}
