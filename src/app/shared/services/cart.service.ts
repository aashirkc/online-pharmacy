import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(


  ) {
    
    this.getItemFromCart()
    const cartData = this.getItemFromCart();
    this.cartLen.next(cartData)
  }

  cartLen = new Subject<any>();

  data: any

  

  watchCart(): Observable<any> {
    return this.cartLen.asObservable();
  }

  addToCart(product: any) {
    this.cartLen.next(product)
    return localStorage.setItem('cart', JSON.stringify(product))
  }

  getItemFromCart() {

    this.data = JSON.parse(localStorage.getItem('cart'));

    return this.data

  }

  removeItemFromCart(i) {
    return localStorage.removeItem(i)
  }

}
