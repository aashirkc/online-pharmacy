import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RatingModule } from 'ngx-bootstrap/rating';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FamousVisitingPlaceServiceService } from './layouts/famous-viting-place/famous-visiting-place-service.service';
import { BannerService } from './shared/services/banner.service';
import { CartService } from './shared/services/cart.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,

  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    CarouselModule,
    HttpClientModule,
    // ToastrModule.forRoot()
  ],
  providers: [
    FamousVisitingPlaceServiceService,
    BannerService, CartService,

    {
      provide: "API_URL",
      useValue: "http://192.168.1.67:8080/"
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
