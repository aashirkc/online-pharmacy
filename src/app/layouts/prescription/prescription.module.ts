import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrescriptionRoutingModule } from './prescription-routing.module';
import { PrescriptionComponent } from './prescription.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PrescriptionComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PrescriptionRoutingModule
  ]
})
export class PrescriptionModule { }
