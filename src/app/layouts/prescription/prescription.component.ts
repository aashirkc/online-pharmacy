import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomService } from 'src/app/shared/services/custom.service';


@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./prescription.component.scss']
})
export class PrescriptionComponent implements OnInit {
  prescription: FormGroup
  public imagePath;
  imageSrc: any;
  message = { status: false, error: false, message: '' };

  fSelected: File;
  constructor(
    private fb: FormBuilder,
    private cus: CustomService,
    private cd: ChangeDetectorRef
  ) {
    this.prescription = this.fb.group({
      images: [''],
      fullname: ['', Validators.required],
      phoneNo: ['', Validators.required],
      email: ['', Validators.required],
      address: ['', Validators.required],
      medicineName: ['',]

    })
  }

  ngOnInit() {
  }
  readURL(event) {
    const fileSelected: File = event.target.files[0];
    this.fSelected = fileSelected
    let img = document.getElementById('photo')

    console.log(fileSelected)
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.imageSrc = event.target.result;

      }

      console.log(reader.readAsDataURL(event.target.files[0]));
    }
  }

  savePres(post) {
    console.log(post)
    let formData = new FormData();
    for (let i in post) {
      if (post[i] == 'null') {
        formData.append(i, '');
      } else {
        formData.append(i, post[i]);
      }

    }
    if (this.fSelected) {
      formData.append('image', this.fSelected);
    }


    this.cus.prescriptionSave(formData).subscribe(x => {
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: x['message'] }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    })
  }
  // onFileChange(event,files) {
  //   var reader1 = new FileReader();
  //   this.imagePath = files;
  //   reader1.readAsDataURL(files[0]);
  //   reader1.onload = (_event) => {
  //     this.imgURL = reader1.result;
  //   }
  // let reader = new FileReader();

  // if(event.target.files && event.target.files.length) {
  //   const [file] = event.target.files;
  //   reader.readAsDataURL(file);

  //   reader.onload = () => {
  //     this.prescription.patchValue({
  //       file: reader.result
  //     });

  // need to run CD since file load runs outside of zone
  // this.cd.markForCheck();
  //   };
  // }
  // }
}
