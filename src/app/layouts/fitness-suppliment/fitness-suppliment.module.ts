import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FitnessSupplimentRoutingModule } from './fitness-suppliment-routing.module';
import { FitnessSupplimentComponent } from './fitness-suppliment.component';

@NgModule({
  declarations: [FitnessSupplimentComponent],
  imports: [
    CommonModule,
    FitnessSupplimentRoutingModule
  ]
})
export class FitnessSupplimentModule { }
