import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessSupplimentComponent } from './fitness-suppliment.component';

describe('FitnessSupplimentComponent', () => {
  let component: FitnessSupplimentComponent;
  let fixture: ComponentFixture<FitnessSupplimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessSupplimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessSupplimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
