import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { CartService } from 'src/app/shared/services/cart.service';
import { CustomService } from 'src/app/shared/services/custom.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private cart: CartService,
    private cus: CustomService,
    private router: Router


  ) {

    this.cartForm = this.fb.group({
      name: [''],
      contactNo: [''],
      address: [''],
      items: this.fb.array([
        this.initgroup()
      ])
    })
  }

  initgroup() {
    return this.fb.group({
      id: [''],
      name: [''],
      quantity: [''],
      total: [''],
      rate: [''],

    })
  }


  addItem() {
    const control = <FormArray>this.cartForm.controls['items'];
    control.push(this.initgroup())
  }





  total = []

  mainTotal
  cartItem: any;
  ngOnInit() {

    this.cartItem = JSON.parse(localStorage.getItem('cart'))
    this.patchData(this.cartItem)

  }

  qnty(e, data, index) {
    console.log(data)
    console.log(e)
    this.total[index] = e * data['rate']
    console.log(this.total[index])

    let sum = 0
    for (let i = 0; i < this.total.length; i++) {
      sum = sum + this.total[i]
    }
    this.mainTotal = sum
  }


  patchData(post) {
    let dt: any = []
    dt['items'] = []
    for (let i = 0; i < post.length; i++) {
      dt['items'][i] = [];
      dt['items'][i].id = post[i].id;
      dt['items'][i].name = post[i].title;
      dt['items'][i].rate = post[i].rate;
      dt['items'][i].total = '';
      dt['items'][i].quantity = '';
      this.addItem

    }
    console.log(dt)

  }

  remove(i) {
    console.log(i)
    let a = this.cart.getItemFromCart()
    console.log(a)
    a.splice(i, 1)
    console.log(a)
    this.cart.addToCart(a)
    this.cartItem = JSON.parse(localStorage.getItem('cart'))
  }

  checkOut() {
    // let a = document.getElementById('exampleInputEmail1')
    let qtosend = []

    for (let i = 0; i < this.cartItem.length; i++) {
      let dt = {}
      dt['quantity'] = document.getElementsByClassName('q' + i)[0]['value']
      // dt['title'] = this.cartItem[i].title
      dt['product_id'] = this.cartItem[i].id
      console.log(dt)
      qtosend.push(dt)
    }
    // console.log(dt)

    this.router.navigateByUrl('/cart/userDetail', { state: { data: qtosend } })
    // this.cartItem.push(qtosend)
    // console.log(this.cartItem)
    // console.log(qtosend)
    // this.cus.orderSave(qtosend).subscribe(x => {
    //   console.log(x)
    // })
  }


  continueShop(){
    this.router.navigateByUrl('/home')
}


}
