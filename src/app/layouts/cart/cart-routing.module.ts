import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart.component';
import { PopupComponent } from './popup/popup.component';

const routes: Routes = [
  {
    path: '',
    component: CartComponent
  },
  {
    path: 'userDetail',
    component: PopupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule { }
