import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { CustomService } from 'src/app/shared/services/custom.service';
import { Router } from '@angular/router';
import { CartService } from 'src/app/shared/services/cart.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  userDetail: FormGroup

  message = { status: false, error: false, message: '' };

  constructor(
    private route: Router,
    private fb: FormBuilder,
    private cus: CustomService,
    private cart: CartService


  ) {
    this.userDetail = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      contact: ['', Validators.required]
    })
  }

  cartItem: any

  ngOnInit() {
    this.cartItem = history.state.data
    console.log(this.cartItem)


  }


  savePres(value) {
    let a = { item: this.cartItem, ...value }
    console.log(a)
 
    let form = new FormData()
    Object.keys(a).forEach(each=>form.append(each,a[each]))
    console.log(form)
    this.cus.orderSave(form).subscribe(x => {
      window.scrollTo(0, 0)
      this.userDetail.reset();
      this.message = { status: true, error: false, message: x['message'] }
      setTimeout(() => {
        this.message['status'] = false
        this.cart.removeItemFromCart("cart")
        this.route.navigate(['/home'])
      }, 3000)

    })
  }

}
