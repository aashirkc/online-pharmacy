import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from './layouts.component';
const routes: Routes = [
  {
    path: '',
    component: LayoutsComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'prefix' },
      { path: 'home', loadChildren: './home/home.module#HomeModule' },
      { path: 'hotel', loadChildren: './hotel/hotel.module#HotelModule' },
      { path: 'featured-product', loadChildren: './featured-product/featured-product.module#FeaturedProductModule' },
      { path: 'fitness-suppliment', loadChildren: './fitness-suppliment/fitness-suppliment.module#FitnessSupplimentModule' },
      { path: 'medicine', loadChildren: './medicine/medicine.module#MedicineModule' },
      { path: 'user', loadChildren: './user/user.module#UserModule' },
      { path: 'hot-product', loadChildren: './hot-product/hot-product.module#HotProductModule' },
      { path: 'first-aids-kits', loadChildren: './first-aid-kits/first-aid-kits.module#FirstAidKitsModule' },
      { path: 'categories/:id', loadChildren: './famous-viting-place/famous-visiting-place.module#FamousVitingPlaceModule' },
      { path: 'prescription', loadChildren: './prescription/prescription.module#PrescriptionModule' },
      { path: 'cart', loadChildren: './cart/cart.module#CartModule' },
      { path: 'view-details', loadChildren: './view-details/view-details.module#ViewDetailsModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }
