import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from 'src/app/shared/services/cart.service';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.scss']
})
export class ViewDetailsComponent implements OnInit {
  message = { status: false, error: false, message: '' };

  constructor(
    private route: ActivatedRoute,
    private cart: CartService
  ) { }

  dataAdapter: any
  description: any

  text: any;

  ngOnInit() {
    window.scrollTo(0, 0)
    console.log(history.state.state.data)
    // this.dataAdapter = history.state.
    this.dataAdapter = history.state.state.data

    var html = this.dataAdapter['body'];
    var div = document.createElement("div");
    div.innerHTML = html;
    this.text = div.textContent || div.innerText || "";
  }



  itemsAddedToCart

  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }
}
