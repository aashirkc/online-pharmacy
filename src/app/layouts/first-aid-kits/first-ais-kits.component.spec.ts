import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstAisKitsComponent } from './first-ais-kits.component';

describe('FirstAisKitsComponent', () => {
  let component: FirstAisKitsComponent;
  let fixture: ComponentFixture<FirstAisKitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstAisKitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstAisKitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
