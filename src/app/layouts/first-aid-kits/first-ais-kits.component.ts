import { Component, OnInit, ViewChild } from '@angular/core';
import { AllCategoriesServicesService } from 'src/app/shared/services/all-categories-services.service';
import { CartService } from 'src/app/shared/services/cart.service';


@Component({
  selector: 'app-first-ais-kits',
  templateUrl: './first-ais-kits.component.html',
  styleUrls: ['./first-ais-kits.component.scss']
})
export class FirstAisKitsComponent implements OnInit {


  constructor(
    private all: AllCategoriesServicesService,
    private cart: CartService,

  ) { }

  message = { status: false, error: false, message: '' };
  firstAid: any
  text: any = []
  ngOnInit() {
    document.body.scrollTop = 0

    this.all.findFirstAidKits().subscribe((data) => {
      this.firstAid = data['data'];
      console.log(this.firstAid['body'])

      for (let i = 0; i < this.firstAid['posts'].length; i++) {
        // var html = this.firstAid['posts'][i]['body'];
        // var div = document.createElement("div");
        // div.innerHTML = html;
        // this.text[i] = div.textContent || div.innerText || "";


        let domparser = new DOMParser()
        let parsedhtml = domparser.parseFromString(this.firstAid['posts'][i]['body'], 'text/html')
        parsedhtml.head.innerHTML += `
        <style>

          p {
            margin-buttom: 30px;
          }
        </style>`

        console.log(parsedhtml)

        this.text[i] = parsedhtml.textContent;
      }
    })
  }

  itemsAddedToCart

  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }

}
