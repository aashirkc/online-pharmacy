import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FirstAidKitsRoutingModule } from './first-aid-kits-routing.module';
import { FirstAisKitsComponent } from './first-ais-kits.component';




@NgModule({
  declarations: [FirstAisKitsComponent],
  imports: [
    CommonModule,
    // SharedModule,
    FirstAidKitsRoutingModule
  ],

})
export class FirstAidKitsModule { }
