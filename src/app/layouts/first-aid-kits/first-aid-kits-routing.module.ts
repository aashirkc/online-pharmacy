import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstAisKitsComponent } from './first-ais-kits.component';

const routes: Routes = [
  {
    path: '',
    component: FirstAisKitsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

exports: [RouterModule]
})
export class FirstAidKitsRoutingModule { }
