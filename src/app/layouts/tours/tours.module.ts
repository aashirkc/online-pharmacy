import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToursRoutingModule } from './tours-routing.module';
import { ToursComponent } from './tours.component';
import { ToursdetailsComponent } from './toursdetails/toursdetails.component';
import { FamousVisitingPlaceServiceService } from '../famous-viting-place/famous-visiting-place-service.service';

@NgModule({
  declarations: [ToursComponent, ToursdetailsComponent],
  imports: [
    CommonModule,
    ToursRoutingModule
  ],
  providers: [FamousVisitingPlaceServiceService]

})
export class ToursModule { }
