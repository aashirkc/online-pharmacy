import { Component, OnInit } from '@angular/core';
import { AllCategoriesServicesService } from 'src/app/shared/services/all-categories-services.service';
import { CartService } from 'src/app/shared/services/cart.service';

@Component({
  selector: 'app-medicine',
  templateUrl: './medicine.component.html',
  styleUrls: ['./medicine.component.scss']
})
export class MedicineComponent implements OnInit {
  itemsPerSlide = 1;
  singleSlideOffset = false;
  noWrap = false;
  message = { status: false, error: false, message: '' };
  constructor(
    private all: AllCategoriesServicesService,
    private cart: CartService
  ) { }
  brands: any
  brandsProduct: any
  ngOnInit() {
    this.all.findMedicine().subscribe((data) => {
      this.brands = data['data']
      // console.log(this.brands['data'])
      this.all.findMedicineProducts(this.brands[3]['id']).subscribe(data => {
        this.brandsProduct = data['data']
        console.log(data)
      });

    })
  }
  p: any

  itemsAddedToCart: any

  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }

  changeProduct(id) {
    this.all.findMedicineProducts(id).subscribe(data => {
      this.brandsProduct = data['data']
    })
  }

}
