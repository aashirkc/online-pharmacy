import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicineRoutingModule } from './medicine-routing.module';
import { MedicineComponent } from './medicine.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [MedicineComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    MedicineRoutingModule
  ]
})
export class MedicineModule { }
