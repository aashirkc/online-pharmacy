import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeaturedProductComponent } from './featured-product.component';

const routes: Routes = [
  {
    path: '',
    component: FeaturedProductComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturedProductRoutingModule { }
