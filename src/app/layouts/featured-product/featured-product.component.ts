import { Component, OnInit } from '@angular/core';
import { BannerService } from 'src/app/shared/services/banner.service';
import { CartService } from 'src/app/shared/services/cart.service';

@Component({
  selector: 'app-featured-product',
  templateUrl: './featured-product.component.html',
  styleUrls: ['./featured-product.component.scss']
})
export class FeaturedProductComponent implements OnInit {
  message = { status: false, error: false, message: '' };
  constructor(
    private home: BannerService,
    private cart: CartService
  ) { }
  featured: any;
  p: any
  filteredDataAdapter: any;
  ngOnInit() {
    window.scrollTo(0, 0)
    this.home.getFeaturedProduct().subscribe(res => {
      console.log(res)
      this.featured = res
      this.filteredDataAdapter = this.featured
      console.log(this.featured)
    })
  }

  itemsAddedToCart

  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }

}
