import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturedProductRoutingModule } from './featured-product-routing.module';
import { FeaturedProductComponent } from './featured-product.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [FeaturedProductComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    FormsModule,
    FeaturedProductRoutingModule
  ]
})
export class FeaturedProductModule { }
