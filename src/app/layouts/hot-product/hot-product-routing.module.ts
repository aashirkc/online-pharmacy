import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotProductComponent } from './hot-product.component';

const routes: Routes = [
  {
    path: "",
    component: HotProductComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotProductRoutingModule { }
