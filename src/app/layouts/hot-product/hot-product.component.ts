import { Component, OnInit } from '@angular/core';
import { BannerService } from 'src/app/shared/services/banner.service';
import { CartService } from 'src/app/shared/services/cart.service';

@Component({
  selector: 'app-hot-product',
  templateUrl: './hot-product.component.html',
  styleUrls: ['./hot-product.component.scss']
})
export class HotProductComponent implements OnInit {
  message = { status: false, error: false, message: '' };

  constructor(
    private hot: BannerService,
    private cart: CartService
  ) { }
  hotProduct: any;
  filteredDataAdapter
  ngOnInit() {
    this.hot.getHotProduct().subscribe((data) => {
      this.hotProduct = data
      this.filteredDataAdapter = this.hotProduct
      console.log(this.hotProduct)
    })
  }
  p: any
  itemsAddedToCart

  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }
}


