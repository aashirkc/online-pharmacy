import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HotProductRoutingModule } from './hot-product-routing.module';
import { HotProductComponent } from './hot-product.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HotProductComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    FormsModule,
    HotProductRoutingModule
  ]
})
export class HotProductModule { }
