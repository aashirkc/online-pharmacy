import { Component, OnInit } from '@angular/core';
import { Users } from 'src/app/shared/model/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public userDetails: Users = new Users();
  constructor() {

  }

  ngOnInit() {
    this.getLoggedInUserDetails();
  }

  getLoggedInUserDetails() {
    if (localStorage.getItem('loggenInUser') == null) {
      this.userDetails = new Users();
    }
    else {
      this.userDetails = JSON.parse(atob(localStorage.getItem('loggenInUser'))) as Users;
    }
  }

}
