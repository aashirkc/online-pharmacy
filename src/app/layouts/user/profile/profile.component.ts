import { Component, OnInit } from '@angular/core';
import { Users } from 'src/app/shared/model/user';
import { UtilityFunctions } from 'src/app/shared/util/utility-functions';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public userDetails: Users = new Users();
  public utility = new UtilityFunctions();
  constructor() { }

  ngOnInit() {
    this.getLoggedInUserDetails();
  }

  getLoggedInUserDetails() {
    if (localStorage.getItem('loggenInUser') == null) {
      this.userDetails = new Users();
    }
    else {
      this.userDetails = JSON.parse(atob(localStorage.getItem('loggenInUser'))) as Users;
    }
  }

}
