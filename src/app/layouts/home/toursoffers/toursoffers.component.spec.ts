import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToursoffersComponent } from './toursoffers.component';

describe('ToursoffersComponent', () => {
  let component: ToursoffersComponent;
  let fixture: ComponentFixture<ToursoffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToursoffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToursoffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
