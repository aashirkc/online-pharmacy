import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CruiseoffersComponent } from './cruiseoffers.component';

describe('CruiseoffersComponent', () => {
  let component: CruiseoffersComponent;
  let fixture: ComponentFixture<CruiseoffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CruiseoffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CruiseoffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
