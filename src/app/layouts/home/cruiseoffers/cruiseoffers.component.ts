import { Component, OnInit } from '@angular/core';
import { BannerService } from 'src/app/shared/services/banner.service';

@Component({
  selector: 'cruiseoffers',
  templateUrl: './cruiseoffers.component.html',
  styleUrls: ['./cruiseoffers.component.scss']
})
export class CruiseoffersComponent implements OnInit {

  constructor(
    private hot: BannerService
  ) { }


  hotProduct: any

  ngOnInit() {
     this.hot.getHotProduct().subscribe((data)=>{
        this.hotProduct = data
        console.log(this.hotProduct)
     })
  }

}
