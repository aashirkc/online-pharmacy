import { Component, OnInit, Inject } from '@angular/core';
import { AllCategoriesServicesService } from 'src/app/shared/services/all-categories-services.service';
import { BannerService } from 'src/app/shared/services/banner.service';
import { CartService } from 'src/app/shared/services/cart.service';
const jquery = window['$']
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  apiUrl: any;
  constructor(
    @Inject('API_URL') apiUrl: string,
    private all: AllCategoriesServicesService,
    private home: BannerService,
    private cart: CartService,
  ) {
   
    this.apiUrl = apiUrl
  }
  categories: any
  banner: any
  featured: any

  ngOnInit() {
    jquery("#owl-hotel-offers").owlCarousel({
      items: 3,
      itemsCustom: false,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [991, 2],
      itemsTablet: [768, 2],
      itemsTabletSmall: [600, 1],
      itemsMobile: [479, 1],
      singleItem: false,
      itemsScaleUp: false,

      //Autoplay
      autoPlay: true,
      stopOnHover: true,

      // Navigation
      navigation: true,
      navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      rewindNav: true,
      scrollPerPage: false,

      //Pagination
      pagination: false,
      paginationNumbers: false,

      // Responsive
      responsive: true,
      responsiveRefreshRate: 200,
      responsiveBaseWidth: window,
    });
    this.all.findAllFamousPlaces().subscribe(d => {
      this.categories = d
      console.log(this.categories)
    })

    this.home.getBanner().subscribe(res => {
      console.log(res)
      this.banner = res['data'].banners
    })
    this.home.getFeaturedProduct().subscribe(res => {
      console.log(res)
      this.featured = res
      console.log(this.featured)
    })



  }
  message = { status: false, error: false, message: '' };

  itemsAddedToCart: any

  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }

}
