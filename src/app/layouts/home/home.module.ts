import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CruiseoffersComponent } from './cruiseoffers/cruiseoffers.component';
import { TestComponent } from './test/test.component';
import { ToursoffersComponent } from './toursoffers/toursoffers.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
@NgModule({
  declarations: [HomeComponent, CruiseoffersComponent, TestComponent, ToursoffersComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CarouselModule
  ]
})
export class HomeModule { }
