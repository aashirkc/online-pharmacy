import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalProductsRoutingModule } from './local-products-routing.module';
import { LocalproductsComponent } from './localproducts.component';
import { LocalproductsdetailsComponent } from './localproductsdetails/localproductsdetails.component';

@NgModule({
  declarations: [LocalproductsComponent, LocalproductsdetailsComponent],
  imports: [
    CommonModule,
    LocalProductsRoutingModule
  ]
})
export class LocalProductsModule { }
