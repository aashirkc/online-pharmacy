import { Component, OnInit, OnChanges, ɵConsole } from '@angular/core';
import { FamousVisitingPlaceServiceService } from './famous-visiting-place-service.service';
import { FamousVisitingPlace, LocalVisitingPlace } from './model/famous-visiting-place';
import { Observable } from 'rxjs';
import { Ng2SearchPipe } from 'ng2-search-filter';
import { ActivatedRoute } from '@angular/router';
import { AllCategoriesServicesService } from 'src/app/shared/services/all-categories-services.service';
import { CartService } from 'src/app/shared/services/cart.service';

@Component({
  selector: 'app-famous-visiting-place',
  templateUrl: './famous-visiting-place.component.html',
  styleUrls: ['./famous-visiting-place.component.scss']
})
export class FamousVisitingPlaceComponent implements OnInit {

  //variable for slider
  itemsPerSlide = 3;
  singleSlideOffset = false;
  noWrap = false;

  p: any
  slidesChangeMessage = '';

  public: FamousVisitingPlace[];

  public famousPlace: Observable<FamousVisitingPlace[]>;
  //helps in filter the search
  public _term: any;
  dataAdapter: any;
  filteredDataAdapter: any;
  public dataAdapter1: Observable<any[]>
  title: any;


  getTerm() {
    return this._term;
  }
  setTerm(value: string) {
    this._term = value;
    this.filteredDataAdapter = this.filterAdapter(value)
  }



  filterAdapter(value) {
    return this.dataAdapter.filter(data => {
      data.title.toLowerCase().indexOf(value.toLowerCase()) !== -1
    })
  }


  message = { status: false, error: false, message: '' };

  public localPlace: LocalVisitingPlace[];


  constructor(private famousPlaceSer: FamousVisitingPlaceServiceService,
    private route: ActivatedRoute,
    private all: AllCategoriesServicesService,
    private cart: CartService
  ) { }
  id: any;

  itemsAddedToCart
  addToCart(product) {
    console.log(product)

    this.itemsAddedToCart = this.cart.getItemFromCart();
    if (this.itemsAddedToCart == null) {
      this.itemsAddedToCart = [];
      this.itemsAddedToCart.push(product)
      this.cart.addToCart(this.itemsAddedToCart)
      window.scrollTo(0, 0)
      this.message = { status: true, error: false, message: 'Items Added To Cart' }
      setTimeout(() => {
        this.message['status'] = false
      }, 3000)
    } else {
      let temp = this.itemsAddedToCart.find(p => p.id == product.id)
      if (temp == null) {

        this.itemsAddedToCart.push(product)
        this.cart.addToCart(this.itemsAddedToCart)
        window.scrollTo(0, 0)
        this.message = { status: true, error: false, message: 'Items Added To Cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)
      } else {
        window.scrollTo(0, 0)
        this.message = { status: true, error: true, message: 'Sorry, this product is already added to cart' }

        setTimeout(() => {
          this.message['status'] = false
        }, 3000)

      }
    }
  }

  ngOnInit() {


    this.route.params.subscribe((data) => {
      console.log(data)
      this.id = this.route.snapshot.paramMap.get('id');
      console.log(this.id)
      // this.getAllPlaces(this.id)
      this.all.findAllCaegoriesData(this.id).subscribe(d => {
        this.title = d
        this.dataAdapter = d['data'].posts
        this.filteredDataAdapter = this.dataAdapter
        console.log(this.dataAdapter)
      })
    })


  }

  // getAllPlaces(){
  //   return this.famousPlaceSer.findAllFamousPlaces().subscribe(data=>{
  //     this.famousPlace = data as FamousVisitingPlace[];
  //   },error=>{
  //     console.log(error);
  //   });
  // }

  getAllPlaces(id) {
    this.dataAdapter1 = this.all.searchFamousPlace(id)
    console.log(this.dataAdapter1)
  }

  getAllLocalPlaces() {
    return this.famousPlaceSer.findAllLocalPlaces().subscribe(data => {
      this.localPlace = data as LocalVisitingPlace[];
    }, error => {
      console.log(error);
    })
  }




}
