import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotelComponent } from './hotel.component';
import { HotelRoutingModule } from './hotel-routing.module';
import { HoteldetailComponent } from './hoteldetail/hoteldetail.component';
import { HotelService } from './hotel.service';

@NgModule({
  declarations: [HotelComponent, HoteldetailComponent],
  imports: [
    CommonModule,
    HotelRoutingModule
  ], providers: [HotelService]
})
export class HotelModule { }
