import { Component, OnInit } from '@angular/core';
import { HotelService } from './hotel.service';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {

  constructor(private hotelService: HotelService) { }

  ngOnInit() {
    this.hotelService.findHotels().subscribe(data => console.log(data), error => console.log(error));
  }

}
