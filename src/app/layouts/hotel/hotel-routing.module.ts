import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HotelComponent } from './hotel.component';
import { HoteldetailComponent } from './hoteldetail/hoteldetail.component';

const routes: Routes = [
  { path: '', component: HotelComponent },
  { path: 'detail', component: HoteldetailComponent }
];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HotelRoutingModule { }
