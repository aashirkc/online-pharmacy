import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Token } from 'src/app/shared/model/token';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) { }

  findHotels() {
    const url = 'http://api.makcorps.com/free/mumbai';
    return this.http.get(url, { headers: this.getHeader() });
  }

  private authHotel() {
    const url = "https://api.makcorps.com/auth";
    const header1 = new HttpHeaders({ 'content-Type': 'application/json' });
    return this.http.post(url,
      {
        "username": "thetryguy",
        "password": "hello123"
      },
      { headers: header1 });
  }


  private getHeader() {
    let token: Token = new Token();
    this.authHotel().subscribe(data => token => data as Token,
      error => console.log(error));
    const accessToken = 'JWT ' + token.value;
    const headerData = new HttpHeaders({ Authorization: accessToken });
    return headerData;
  }
}
