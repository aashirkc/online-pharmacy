import { Component, OnInit } from '@angular/core';
import { Users } from 'src/app/shared/model/user';
import { SignupService } from './signup.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';
import { Token } from 'src/app/shared/model/token';
import { Pattern } from 'src/app/shared/pattern';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public user: Users = new Users();
  message = { status: false, error: false, message: '' };
  protected pattern: Pattern = new Pattern();

  constructor(private signupService: SignupService
    , private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  onSignup(form) {
    console.log(form)
    // show spinner for processing
    this.spinner.show();

    // get user input value
    this.user = form.value as Users;

    console.log(this.user)

    this.signupService.signupUser(this.user).subscribe(data => {
      console.log(data)
      console.log("hey")
      const tokenData = data as Token;
      this.message = { status: true, error: false, message: data['message'] };
    }, error => {
      // const message = error as HttpErrorResponse;
      // this.message = { status: true, error: true, message: message.error.message };
    });

    // remove spinner after 1.2 seconds
    setTimeout(() => {
      this.spinner.hide();
    }, 1200);
  }

}
