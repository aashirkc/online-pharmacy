import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from 'src/app/shared/services/api-end-points';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }

  signupUser(user) {
    const url = ApiEndPoints.SIGNUP_ENDPOINT;
    // const header1 = new HttpHeaders({ 'content-Type': 'application/json' });

    console.log(url)

    return this.http.post(url, user)


  }
}
