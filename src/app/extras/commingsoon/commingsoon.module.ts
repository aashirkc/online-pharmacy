import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommingsoonRoutingModule } from './commingsoon-routing.module';
import { CommingsoonComponent } from './commingsoon.component';

@NgModule({
  declarations: [CommingsoonComponent],
  imports: [
    CommonModule,
    CommingsoonRoutingModule
  ]
})
export class CommingsoonModule { }
