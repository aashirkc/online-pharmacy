import { Component, OnInit } from '@angular/core';
import { Users } from 'src/app/shared/model/user';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Token } from 'src/app/shared/model/token';
import { CustomService } from 'src/app/shared/services/custom.service';
import { HttpErrorResponse } from '@angular/common/http';
import { StorageService } from 'src/app/shared/services/storage/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  protected userModel: Users = new Users();
  private token: Token;

  message = {
    status: false,
    error: false,
    message: ''
  };

  constructor(public router: Router,
    private loginService: LoginService,
    private spinner: NgxSpinnerService,
    private customService: CustomService,
    private storageService: StorageService) { }

  ngOnInit() {
  }

  onLoggedin(form) {

    /** spinner starts on login attempt */
    this.spinner.show();

    this.userModel = form.value as Users;

    this.loginService.authenticateUser(this.userModel).subscribe(tokenData => {
      this.token = JSON.parse(JSON.stringify(tokenData));
      localStorage.setItem('token', this.token.value);
      this.customService.getUserDetailsFromToken(this.token.value).subscribe(userData => {
        const userDetails = userData as Users;
        userDetails.password = '';
        this.storageService.setItem('loggenInUser', btoa(JSON.stringify(userDetails)));
        this.router.navigate(['/home']);
      }, error => { console.log(error); });
    },
      error => {
        const message = error as HttpErrorResponse;
        this.message = { status: true, error: true, message: message.error.message };
        setTimeout(() => {
          this.message.status = false;
        }, 6000);
      }
    );

    // remove spinner after 1.2 seconds
    setTimeout(() => {
      this.spinner.hide();
    }, 1500);
  }

}
