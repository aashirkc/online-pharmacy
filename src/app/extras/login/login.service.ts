import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from 'src/app/shared/services/api-end-points';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  authenticateUser(userModel) {
    const loginUrl = ApiEndPoints.LOGIN_ENDPOINT;
    const headersData = new HttpHeaders({ 'content-Type': 'application/json' });
    return this.http.post(loginUrl, userModel, { headers: headersData });
  }
}
